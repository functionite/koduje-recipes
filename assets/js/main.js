(function () {
  'use strict';
  
  var items = document.querySelectorAll('.item'),
    menuItems = document.querySelectorAll('.menu-item');
  
  function setActiveNav(activeMenuItem) {
    menuItems.forEach(function (item) {
      item.parentNode.classList.remove('active');
    });
    activeMenuItem.parentNode.classList.add('active');
  }
  
  function clikableNavigation() {

    menuItems.forEach(function (item) {
      item.addEventListener("click", function () {
        setActiveNav(item);
      });
    });
  }
  
  function clearInputs() {
    
    document.querySelectorAll('.search-input, #mce-EMAIL')
      .forEach(function (element) {
        var defalutValue = "Wyszukaj";
      
        if (element.id === "search-input") {
          defalutValue = "Wyszukaj";
        } else if (element.id === "mce-EMAIL") {
          defalutValue = "Twój Adress E-mail";
        }
          
        element.addEventListener("focus", function () {
          if (element.value === defalutValue) {
            element.value = "";
          }
        });
      
        element.addEventListener("blur", function () {
          if (element.value === "") {
            element.value = defalutValue;
          }
        });
      
      });
  }

  function popupNewsletterForm() {
    
    document.querySelector('#show-newsletter-form')
      .addEventListener("click", function () {
        document.querySelectorAll('#dark-background, #newsletter-container')
          .forEach(function (element) {
            element.classList.remove('hidden-on-mobile');
          });
        document.querySelector('.newsletter-input').focus();
      });
  }
  
  function closeNewsletterForm() {
    
    document.querySelector('#close-form')
      .addEventListener("click", function () {
        document.querySelectorAll('#dark-background, #newsletter-container')
          .forEach(function (element) {
            element.classList.add('hidden-on-mobile');
          });
      });
  }
  
  function searchInputAnimation() {
    document.querySelector('#search-icon')
      .addEventListener("click", function () {
        document.querySelectorAll('#search-icon, #search-input, #logo-wrapper')
          .forEach(function (element) {
            element.classList.toggle('search-closed');
          });
        document.querySelector('#search-input').focus();
      });
  }
  
  function tagsPopup() {
    document.querySelectorAll('.tag-icon, .tags')
      .forEach(function (element) {
        element.addEventListener("click", function () {
          document.querySelectorAll('.tag-icon, .tags')
            .forEach(function (element) {
              element.classList.toggle('tags-closed');
            });
        });
      });
  }
  
  function newsletterHide() {
    window.addEventListener("scroll", function () {
      var newsletterDiv = document.querySelector('.newsletter-mobile-container');
      if (window.pageYOffset === 0) {
        newsletterDiv.classList.remove('closed');
      } else {
        newsletterDiv.classList.add('closed');
      }
    });
  }
    
  function sortItems() {
    
    var collumns = document.querySelectorAll('.item-collumn'),
      collumnsNumber = 4,
      shortestCollumn = 0,
      collumnHeights = [0, 0, 0, 0],
      acceptedHeightDifference = 50;
    
    function getlowestValue(values) {
      var lowestVal = 0, i = 0;
      for (i; i < collumnsNumber; i += 1) {
        if (values[lowestVal] > values[i] + acceptedHeightDifference) {
          lowestVal = i;
        }
      }
      return lowestVal;
    }
    
    function getCollumnNumber() {
      function isCollumnHidden(collumn) {
        return (window.getComputedStyle(collumn, null).display === 'none');
      }
      if (isCollumnHidden(collumns[1])) { return 1; }
      if (isCollumnHidden(collumns[2])) { return 2; }
      if (isCollumnHidden(collumns[3])) { return 3; }
      return 4;
    }
    
    collumnsNumber = getCollumnNumber();
    items.forEach(function (item) {
      shortestCollumn = getlowestValue(collumnHeights);
      collumns[shortestCollumn].appendChild(item);
      collumnHeights[shortestCollumn] += item.scrollHeight;
    });
  }
  
  function tagSystem() {
    var possibleTags = ['book', 'source', 'people', 'meetjs', 'conference', 'video',
               'html', 'css', 'js', 'fb-groups', 'portals', 'blogs', 'poland', 'world'],
      urlTags = [],
      tagsOK = false,
      refreshTags;
    
    
    function removeTagLabels(labels) {
      if (labels) {
        labels.forEach(function (tagLabel) {
          tagLabel.parentElement
            .parentElement
            .removeChild(tagLabel.parentElement);
        });
      }
    }
    
    function activeTagsRefresh() {
      var container = document.querySelector('.active-tags'),
        tagLabels = document.querySelectorAll('.a-tag'),
        newHref;
      
      removeTagLabels(tagLabels);
      
      if (tagsOK) {
        newHref = "#";
        urlTags.forEach(function (uTag) {
          newHref += uTag;
          var newTagLabel = document.createElement("a");
          newTagLabel.textContent = uTag;
          newTagLabel.classList.add("a-tag", "refreshTags");
          newTagLabel.setAttribute('href', newHref);
          newTagLabel.addEventListener("click", function () {
            refreshTags(newTagLabel.getAttribute('href'));
          });
          container.appendChild(
            document.createElement("li")
              .appendChild(newTagLabel).parentElement
          );
          newHref += ";";
          container.style.display = "flex";
        });
        
      } else {
        container.style.display = "none";
      }
    }
    
    function proposedTagsRefresh() {
      
      
      var propose = possibleTags.slice(0),
        container = document.querySelector('.tags'),
        tagLabels = document.querySelectorAll('.tag');
    
      function chooseTags() {
        if (tagsOK) {
          urlTags.forEach(function (uTag) {
            propose.splice(propose.indexOf(uTag), 1);
          });
        } else {
          propose = ['book', 'source', 'people', 'meetjs', 'conference', 'video'];
        }
      }
      
      removeTagLabels(tagLabels);
      
      function createNewLabels() {
        propose.forEach(function (tagToDisplay) {
          var newTagLabel = document.createElement("a"),
            newHref = "";
 
          if (tagsOK) {
            newHref += "#";
            urlTags.forEach(function (uTag) {
              newHref += uTag + ";";
            });
            newHref += tagToDisplay;
          } else {
            newHref = "#" + tagToDisplay;
          }
          
          newTagLabel.textContent = tagToDisplay;
          newTagLabel.classList.add("tag", "refreshTags");
          newTagLabel.setAttribute('href', newHref);
          newTagLabel.addEventListener("click", function () {
            refreshTags(newTagLabel.getAttribute('href'));
          });
          container.appendChild(
            document.createElement("li")
              .appendChild(newTagLabel).parentElement
          );

        });
      }
      
      removeTagLabels();
      chooseTags();
      createNewLabels();
    }
    
    function updateNavbar() {
      
      urlTags.forEach(function (uTag) {
        if (uTag === "book") { setActiveNav(menuItems[1]); }
        if (uTag === "source") { setActiveNav(menuItems[2]); }
        if (uTag === "people") { setActiveNav(menuItems[3]); }
        if (uTag === "meetjs") { setActiveNav(menuItems[4]); }
        if (uTag === "conference") { setActiveNav(menuItems[5]); }
        if (uTag === "video") { setActiveNav(menuItems[6]); }
      });
      if (!tagsOK) { setActiveNav(menuItems[0]); }
    }
    
    refreshTags = function (href) {

      function lookForValidTags() {
        var found = 0;
        urlTags.forEach(function (uTag) {
          possibleTags.forEach(function (pTag) {
            if (uTag === pTag) {
              found += 1;
            }
          });
        });
        return found;
      }
      
      function containsAllTags(classList) {
        var allTagsfound = true;
        
        urlTags.forEach(function (uTag) {
          if (!classList.contains("tag-" + uTag)) {
            allTagsfound = false;
          }
        });
        return allTagsfound;
      }
      
      if (href.split("#")[1]) {
        urlTags =  href.split("#")[1].split(";");
        if (urlTags.length === lookForValidTags()) {
          tagsOK = true;
        } else {
          tagsOK = false;
        }
      } else {
        tagsOK = false;
      }
      
      var displayedAmount = 0;
      items.forEach(function (item) {
        
        if (tagsOK) {
          if (containsAllTags(item.classList)) {
            item.style.display = "block";
            displayedAmount += 1;
          } else {
            item.style.display = "none";
          }
        } else {
          //W Przypadku nieprawidłowych Tagów wyświetla wszystko
          
          item.style.display = "block";
        }
      });
      
      if (!displayedAmount && tagsOK) {
        document.querySelector('.no-items').style.display = "block";
      } else {
        document.querySelector('.no-items').style.display = "none";
      }
      
      sortItems();
      proposedTagsRefresh();
      activeTagsRefresh();
      updateNavbar();
    };
    
    function executeWhenNeeded() {
      document.querySelectorAll('.refreshTags')
        .forEach(function (tagLink) {
          tagLink.addEventListener("click", function () {
            refreshTags(tagLink.getAttribute('Href'));
            
          });
        });
      document.addEventListener("hashchange",  function () {
        refreshTags(document.URL);
      });
      window.addEventListener("resize", function () {
        refreshTags(document.URL);
      });
    }
    
    refreshTags(document.URL);
    executeWhenNeeded();
  }
  
  function fixItemsOnResize() {
    window.addEventListener("resize", function () {
      sortItems();
      
    });
  }
  
  function main() {
    clikableNavigation();
    clearInputs();
    popupNewsletterForm();
    closeNewsletterForm();
    tagsPopup();
    searchInputAnimation();
    newsletterHide();
    tagSystem();
  }
  
  main();
  
})();
